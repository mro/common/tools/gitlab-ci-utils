#!env python3

import sys
import re
import json

LineRe = re.compile(r'^(?P<file>[/0-9a-zA-Z+._-]{1,}):(?P<line>[0-9]{1,}):((?P<char>[0-9]{1,}):)?\s*(?P<message>.*)$')

def mkcodeclimate(file, out):
  out.write('[\n');
  first = True
  for line in file:
    m = LineRe.search(line)
    if m:
        if not first:
            out.write(',\n')
        first = False

        lower = line.lower()
        severity = 'major' if 'warning' in lower else 'minor'
        severity = 'critical' if 'error' in lower else severity

        fingerprint = m.group('file') + ':' + m.group('line')

        out.write('  ')
        out.write(json.dumps({
            'description': m.group('message'),
            'fingerprint': fingerprint,
            'severity': severity,
            'location': { 'path': m.group('file'), 'lines': { 'begin': int(m.group('line')) }}
        }))
  out.write('\n]\n')

if __name__ == "__main__":
  mkcodeclimate(sys.stdin, sys.stdout)
