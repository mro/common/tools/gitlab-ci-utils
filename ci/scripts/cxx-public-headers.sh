#!/bin/bash
# Author: Sylvain Fargier <fargier.sylvain@gmail.com>

BINDIR=$(cd $(dirname $0); pwd)
. "${BINDIR}/.functions.sh"

INSTROOT="$1"
CC=${CC:-g++}
CFLAGS="-std=c++11 ${EXTRA_CFLAGS}"

[ -d "$INSTROOT" ] || die "Usage: cxx-public-headers.sh instroot_dir"

INCDIRS=$(find "$INSTROOT" -iname include -type d)
HDRS=
ERROR_COUNT=0
TMP=$(mktemp "cxx-public-headers_XXXXXX.cc")

for d in $INCDIRS; do
  CFLAGS="$CFLAGS -isystem $d"
  hdrs=$(cd "$d"; find . -iname "*.hpp" -o -iname "*.h" -o -iname "*.hh" | sed -e 's#\./##')
  HDRS="$HDRS $hdrs"
done

for h in $HDRS; do
  ebegin "Testing \"$h\""
  echo "
  #include <$h>

  int main() { return 0; }
  " > "${TMP}"
  RET=$(${CC} -E "${TMP}" ${CFLAGS} 2>&1 > /dev/null)
  eend $? || { echo "${RET}"; ERROR_COUNT=$(( $ERROR_COUNT + 1 )); }
done

rm -f "${TMP}"
exit $ERROR_COUNT