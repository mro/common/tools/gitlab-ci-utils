# Parts of this script comes from gentoo's /sbin/functions.sh
# Author: Sylvain Fargier <fargier.sylvain@gmail.com>

# Setup COLS and ENDCOL so eend can line up the [ ok ]
COLS="${COLUMNS:-0}"		# bash's internal COLUMNS variable
(( COLS == 0 )) && COLS="$(set -- `stty size 2>/dev/null` ; echo "$2")"
(( COLS > 0 )) || (( COLS = 80 ))	# width of [ ok ] == 7

if [[ ${RC_ENDCOL} == "no" ]] ; then
	ENDCOL=''
else
#	ENDCOL=$'\e[A\e['$(( COLS - 8 ))'C'
	ENDCOL=$'\e['$(( COLS - 7 ))'G'
fi

[ -t 1 ] || RC_NOCOLOR="yes"
# Setup the colors so our messages all look pretty
if [[ ${RC_NOCOLOR} == "yes" ]] ; then
	unset GOOD WARN BAD NORMAL HILITE BRACKET
else
	GOOD=$'\e[32;01m'
	WARN=$'\e[33;01m'
	BAD=$'\e[31;01m'
	HILITE=$'\e[36;01m'
	BRACKET=$'\e[34;01m'
	NORMAL=$'\e[0m'
fi

END_OK="${ENDCOL} ${BRACKET}[ ${GOOD}ok${BRACKET} ]${NORMAL}"
END_NOK="${ENDCOL} ${BRACKET}[ ${BAD}!!${BRACKET} ]${NORMAL}"

function die()
{
  echo -e " ${BAD}!!!${NORMAL} $*" >&2
  exit 1
}

function ebegin()
{
  echo -ne " ${GOOD}*${NORMAL} $* ... "
}

function eend()
{
  local retval="${1:-0}" msg="${2}"

  [ -n "$msg" ] && msg="${BAD}Error${NORMAL}: $msg"
  if [ "$retval" == "0" ]; then
    echo -e "$msg $END_OK"
  else
    echo -e "$msg $END_NOK"
  fi
  return $retval
}

function eerror()
{
  echo -e " ${BAD}*${NORMAL} $*"
}